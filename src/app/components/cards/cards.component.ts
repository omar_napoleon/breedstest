import { Component, Input } from '@angular/core';

import { Router} from '@angular/router';
import { timeout } from 'rxjs/operators';

@Component({
  selector: 'app-cards',
  templateUrl: './cards.component.html',
  styleUrls: ['./cards.component.css']
})
export class CardsComponent {

@Input() items: any[];
@Input() imageBreeds: any[];

msgModal: boolean = false;

  constructor(private router: Router) { }

  seeSubBreeds( breed: string , subBreed: string []) {
    console.log(breed);
    if  (subBreed.length > 0) {
      this.router.navigate(['/artist', breed]);
    } else {
      this.msgModal = true;
      setTimeout(() => {this.msgModal = false }, 2000);
    }
  }

}

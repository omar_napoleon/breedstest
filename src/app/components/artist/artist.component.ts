import { Component, OnInit } from '@angular/core';

import { ActivatedRoute} from '@angular/router';
import { AnimalsService } from '../../services/animals.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styleUrls: ['./artist.component.css']
})
export class ArtistComponent {

  subBreeds: string[] = [];
  imageBreed: any[] = [];
  loading: boolean;

  constructor(private router: ActivatedRoute, private breeds: AnimalsService) {

    this.router.params.subscribe(params => {
      this.getSubBreed(params['id']);
    });
  }

    getSubBreed(id: string) {
    this.breeds.getSubBreeds(id)
      .subscribe(data => {
        this.subBreeds = data;
      });
    this.img(id);
    this.loading = false;
    }
    img( img: string ) {
      this.breeds.getImg(img)
      .subscribe((data: any[]) =>  {
        for (let sub of this.subBreeds) {
          for (let aux  of data) {
            if (aux.includes(img+'-'+sub)) {
              this.imageBreed.push(aux);
              break;
            }
          }
        }
        console.log(this.imageBreed.length);
        console.log(this.imageBreed);
      });
  }
}

import { Component, Input } from '@angular/core';
import { AnimalsService } from '../../services/animals.service';



@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})

export class SearchComponent {

  subBreeds: string[] = [];
  imageBreed: any[] = [];
  loading: boolean;

  constructor(private breeds: AnimalsService  ) { 

  }

    search( term: string ) {
      this.loading = true;
      this.breeds.getSubBreeds(term)
      .subscribe((data: any) =>  {
        this.subBreeds = data;
      });
      this.img(term);
      this.loading = false;

    }
    img( img: string ) {
      this.breeds.getImg(img)
      .subscribe((data: any[]) =>  {
        for (let sub of this.subBreeds) {
          for (let aux  of data) {
            if (aux.includes(img+'-'+sub)) {
              console.log(img);
              console.log('esntro2');
              this.imageBreed.push(aux);
              break;
            }
          }
        }
        console.log(this.imageBreed.length);
        console.log(this.imageBreed);
      });
    }
  }


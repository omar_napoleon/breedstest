import { Component, OnInit } from '@angular/core';
import { AnimalsService } from '../../services/animals.service';
import { empty } from 'rxjs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  breedsDogs: any;
  imageBreed: any[] = [];
  loading: boolean;

  breedDogObject: object = {
  name: String,
  sub: [],
  img: []
  };


  constructor(private breeds: AnimalsService  ) {
    this.loading = true;
    this.breeds.getBreeds()
    .subscribe((data: any) =>  {
      this.breedsDogs = data;
      /*this.breedsDogs = Object.keys((data));*/
      for ( let item of Object.keys((this.breedsDogs))) {
        this.img(item);
      }
      console.log(this.imageBreed);
      console.log(this.breedsDogs);
      this.loading = false;
    });
  }
  img( img: string ) {
    this.breeds.getImg(img)
    .subscribe((data: any[]) =>  {
      let aux: string;
      if (data) {
        aux = data[0];
        this.imageBreed.push(aux);
       // console.log('HOLA   ' + aux);
      } else {
        aux = data[0];
        this.imageBreed.push(aux);
        console.log('CHAO   ' + aux);
      }
    });
  }
}

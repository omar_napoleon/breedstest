import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';

import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class AnimalsService {

  constructor( private http: HttpClient) {

  }

  getQuery(query: string){
    const URL =`https://dog.ceo/api/${query}`;

    return this.http.get(URL);
  }

  getBreeds() {
    return this.getQuery('breeds/list/all')
     .pipe( map( data => data['message']));
  }

  getImg(dogName: string) {
    return this.getQuery(`breed/${dogName}/images`)
    .pipe( map( data => data['message']));
  }

  getSubBreeds(breed: string) {
    return this.getQuery(`breed/${breed}/list`)
      .pipe( map( data => data['message']));
  }
}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'noimages'
})
export class NoimagesPipe implements PipeTransform {

  transform(images: any): string {
    if (!images ) {
      return 'assets/img/noimage.png';
    } else {
      return images;
    }
  }

}
